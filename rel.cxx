#encoding "utf-8"    
#GRAMMAR_ROOT F 

NotPlace -> Word <h-reg1, kwset=~["city", "adress"]>;

FirstName -> NotPlace <gram = "persn">;
FamilyName -> NotPlace <gram = "famn">;
PatrnName -> NotPlace <gram = "patrn">;
Uname -> NotPlace <kwtype = "names"> {outgram = "sg"} ;
//Uname -> Word<h-reg1, ~fw,~dict>;
FirstName -> Uname;
FamilyName -> Uname;
PatrnName -> Uname;

FOA -> Word <kwtype = adress>;
FOA -> FOA Conj FOA;
FOA -> DescrAdj FOA;
Nob_part -> Word <kwtype = "nob_part">;
Adjec -> 'старший' | 'младший' | 'средний' | 'великий';
ShortName -> Word<wff=/[А-Я]./>;
ShortName -> Word<wff=/(I|II|III|IV|V|VI|VII|VIII|IX|X)/>;

FamilyName ->  Nob_part NotPlace<gram="famn",rt>;
FamilyName -> Nob_part NotPlace<kwtype="names",rt>;

SimpleName -> FirstName | FamilyName | PatrnName | Uname;
Name -> FirstName<nc-agr[1],rt> FamilyName<nc-agr[1]>;
Name -> FamilyName<nc-agr[1]> FirstName<nc-agr[1],rt>;
Name -> FirstName<nc-agr[1],rt> PatrnName<nc-agr[1]>;
Name -> FamilyName<nc-agr[1]> FirstName<nc-agr[1],rt> PatrnName<nc-agr[1]>;
Name -> FirstName<nc-agr[1],rt> PatrnName<nc-agr[1]> FamilyName<nc-agr[1]>;
Name -> ShortName (ShortName) FamilyName<rt>;
Name -> FirstName<rt> ShortName;
Name -> FirstName<gnc-agr[1],rt> FirstName<gnc-agr[1]>;
Name -> FirstName<nc-agr[1],rt> PatrnName<nc-agr[1]> Word<h-reg1, nc-agr[1]>;



Name-> SimpleName;
FOAName -> FOA<nc-agr[1]> Name<nc-agr[1], rt>;
FOAName -> FOA Name<kwtype="names">;

Name -> SeniorJunior<gnc-agr[1]> Word<rt, gram="famn", gnc-agr[1]>;  // старший Иванов
Name -> SeniorJunior<gnc-agr[1]> Word<rt, gram="patrn", gnc-agr[1]>; // старший Петрович

Name -> SeniorJunior 'из' Word<gram="famn,pl,gen">;  // старший Иванов
Name -> SeniorJunior 'из' Word<gram="patrn,pl,gen">;
Conj -> 'и' | Comma;
Name -> AdjCoord<h-reg1> Name;

FUnch -> Full_Name<GU =&[ "nom, acc, dat, gen, ins"]> interp (Fact.FPerson::not_norm);
SUnch -> Full_Name<gram = "nom, acc, dat, gen, ins"> interp (Fact.SPerson::not_norm);

FUnch -> Full_Name <GU=~["nom, acc,dat,gen,ins"]>;
SUnch -> Full_Name <GU=~["nom, acc,dat,gen,ins"]>;

Full_Name -> Name;
Full_Name -> FOAName;


//группа прилагательного
SimpleAdj -> Word<gram="A"> | Word <gram="APRO">;
SpSimpleAdj -> SeniorJunior;
SpSimpleAdj -> DescrAdj;
SpSimpleAdj -> AnafAdj;

AdjCoord -> SimpleAdj;
AdjCoord -> SpSimpleAdj;
AdjCoord -> (AdvCoord) SimpleAdj<rt> (AdvCoord);
AdjCoord -> Word<gram="APRO">;
AdjCoord -> AdjCoord AdjCoord+[gnc-agr];
AdjCoord -> AdjCoord<gnc-agr[1],rt> Conj AdjCoord<gnc-agr[1]>;
SeniorJunior -> 'младший' | 'старший' | 'средний'| 'первый' | 'последний';
DescrAdj -> 'великий';
AnafAdj -> 'свой' | 'его' | 'её';

Part -> Word<gram="partcp">;
Part -> AdjCoord;
Part -> 'который' <gram="sg">;

//причастный оборот
PartP -> Comma Part Word* Comma; 
PartP -> Comma Word<gram="PR"> Part Word* Comma;

//группа наречия
CompAdv -> Adv<gram="comp">; 
AdvCoord -> Adv;
AdvCoord -> Adv Adv+;
AdvCoord -> AdvCoord Conj AdvCoord;

//деепричастный оборот
Ger -> (AdvCoord) Word<gram="ger">;
GerP -> Comma Ger Word* Comma;

VRel -> VP<kwtype="rel_v"> interp (Fact.Rel);
VBeing -> VP<kwtype="being">;
VBorn -> VP<kwtype="rel_v_born">;
SimpleVerb -> Verb;

VP -> (AdvCoord) SimpleVerb<rt> (AdvCoord);
VP -> VP<rt> GerP;
VP -> VP PP;
BeingSmb -> (Adv) VP<kwtype="being"> NPRole<gram="ins"> ;

//существительное
N -> Noun<gram = "~persn,~famn,~patrn"> ;
NP -> N;
PP -> Prep<cut> NP<rt>;
NP -> PP;
NP -> NP PartP;
NP -> NP<rt> PP;
NP -> AdjCoord<gnc-agr[1]> NP<rt,gnc-agr[1]>;
NP -> NP<gnc-agr[1],rt> AdjCoord<gnc-agr[1]>;
NP -> AdvCoord NP<rt>;
NP -> NP<rt> NP<gram="gen">;
Appos -> NP<gnc-agr[1],rt> '(' NP<gnc-agr[1]> ')';
Appos -> NP<gnc-agr[1],rt> Hyphen NP<gnc-agr[1]>;
Appos -> NP<gnc-agr[1],rt> Comma NP<gnc-agr[1]> Comma;
Appos -> NP<gnc-agr[1]> Comma NP<gnc-agr[1]> '.';

NPRel -> NP<kwtype="conn"> interp (Fact.Rel) ;
NPRole -> Noun<kwtype="role"> interp (Fact.Rel);
NPRole -> SeniorJunior NPRole;
NPRole -> AdjCoord NPRole;
NP -> Appos;

FNPP -> Full_Name interp (Fact.FPerson);
FNPP -> FUnch;
FNP -> FNPP;
FNP -> FNPP PP;
FNP -> FNPP<gnc-agr[1],rt> AdjCoord<gnc-agr[1]>;
FNP -> FNPP<rt> NP<gram="gen">;
Appos -> FNPP<gnc-agr[1],rt> '(' NP<gnc-agr[1]> ')';
Appos -> FNPP<gnc-agr[1],rt> Hyphen NP<gnc-agr[1]>;
Appos -> FNPP<gnc-agr[1],rt> Comma NP<gnc-agr[1]> Comma;
Appos -> FNPP<gnc-agr[1]> Comma NP<gnc-agr[1]> '.';
FNP -> Appos;
FNP -> FNP PartP;


SNPP -> Full_Name interp (Fact.SPerson);
SNPP -> SUnch;
SNP -> AdjCoord<gnc-agr[1]> SNPP<rt,gnc-agr[1]>;
SNP -> AdvCoord SNPP<rt>;
SNP -> SNPP;
SNP -> SNPP PP;
//F -> FNP "и" SNP Verb<gram="pl">;
//F -> FNP VP SNP;
//F -> FNP Prep SNP;

F -> FNP 'и' SNP  BeingSmb ;
F -> FNP '-' NPRole SNP<gram="gen">;
F -> NPRole SNP<gram="gen"> '-' FNP;
//F -> NPRole SNP<gram="gen">  FNP ;
F -> NPRole SNP<gram="gen"> Comma  FNP Comma;
F -> NPRole SNP<gram="gen"> Comma  FNP EOSent;

F -> NPRel Word <gram="PR"> FNP 'и' SNP ;
//F -> NPRole NP<nc-agr[0]> FNP<nc-agr[0]> Comma NP<nc-agr[0]> SNP<nc-agr[0]>;
F -> FNP 'и' AnafAdj NPRole SNP;
F -> FNP 'и' AnafAdj NPRole Comma SNP;

//F-> FNP 'и' SNP 
F -> NPRole Word<h-reg1, gram="gen"> interp (Fact.SPerson) Word<h-reg1> interp (Fact.SPerson) ;
F -> Word<h-reg1> Comma NPRole Word<h-reg1, gram= "gen">;  
F -> FNP<c-agr[0]> Comma NPRole<c-agr[0]>SNP<gram="gen">;
F -> FNP VRel SNP<gram="dat"> ;
F -> FNP VRel  SNP<gram="dat"> ;
F -> FNP BeingSmb SNP;
F -> NPRel FNP Word<gram="PR"> SNP ;
F -> FNP 'и' NPRole<gnc-agr[1]> Comma SNP<gnc-agr[1]>;
//F -> FNP 'и' NPRole<gnc-agr[1]> SNP<gnc-agr[1]>;
F -> FNP<sp-agr[0]> VRel<sp-agr[0]> Word<gram="PR"> SNP ;
F -> FNP<sp-agr[0]> VRel<sp-agr[0]> SNP ;
F -> FNP Part<kwtype="rel_adj"> interp (Fact.Rel) Word<gram="PR"> SNP; 
F -> FNP Prep AnafAdj  NPRole SNP ;
//F -> FNP Comma NPRole SNP 'и' SNP interp (Fact.FPerson = "prev") ;

SNPpl -> SNP Comma SNP ;
SNPpl -> SNP  'и' SNP ;
SNPpl -> SNP Comma SNPpl+;

F -> NPRole<gram="pl"> "-" SNPpl interp (Fact.FPerson="smth");
F-> U;
//F -> FOA interp (Fact.FPerson) "и" FOA(Fact.SPerson) Name interp (+Fact.FPerson; +Fact.SPerson);
//F -> FNP<sp-agr[0]> VRel<sp-agr[0]>SNP CompPhrase interp (+Fact.Rel);
//F -> FNP<gram = "gen">  VBeing  NPRole "по" "имя" Word<h-reg1> interp (Fact.SPerson); 
//F -> VP <kwtype="rel_v_born">  Word<gram = "PR"> FNP NPRole<gnc-agr[1]>NP<gnc-agr[1]>;
//F -> NPRole FNP interp (Fact.SPerson = " ");
//ROOT -> F;

//silmarillion patterns


CapsName -> AnyWord<wff=/[A-ЯЁ]+/>;
Role -> Noun<kwtype="role">;
Na -> AnyWord<wff=/[А-Я][а-яё]+/>;
Nam -> Na Na*;

U -> CapsName interp (Fact.FPerson)"—" AnyWord* Comma Role<gram = "nom"> interp (Fact.Rel) Nam interp (Fact.SPerson);
U -> CapsName interp (Fact.FPerson; Fact.FPerson)"—" AnyWord* Comma Role<gram="nom"> interp (Fact.Rel) Nam interp (Fact.SPerson) "и" Role<gram="nom"> interp (Fact.Rel) Nam interp (Fact.SPerson);

U -> CapsName interp (Fact.FPerson) "—" AnyWord* Role<gram = "nom"> interp (Fact.Rel) Nam interp (Fact.SPerson; Fact.FPerson) Comma Role<gram="gen"> Nam interp (Fact.SPerson);







