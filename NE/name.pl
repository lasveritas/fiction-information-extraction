#!/usr/bin/perl
use utf8;
use strict;
use Data::Dumper;

$Data::Dumper::Useqq = 1;{
 no warnings 'redefine';
 sub Data::Dumper::qquote {
   my $s = shift;
   return "'$s'";
 }
}

open (FILE, "out.txt") or die "Sorry, but i can't open 'out.txt'";
open (NAMES, ">dicts/names.txt") or die;
open (CITY, '>dicts/city.txt') or die;

binmode(FILE, ":encoding(utf-8)");
binmode(STDOUT, ":encoding(utf-8)");
binmode(STDERR, ":encoding(utf-8)");

binmode(NAMES, ":encoding(utf-8)");
binmode(CITY, ":encoding(utf-8)");

my ($num_of_nn, $line,@s,$i,$name, $name_not_norm, %city, %pers, %nom, %norm, %unorm, @a, %h, %justnames, %male, %female, %strong, %per, %p, %persn);
@s=<FILE>;
for ($line = 0; $line < scalar(@s) ; $line++ ){
    if ($i = index($s[$line], "Name = ") != -1){
      $s[$line] =~ s/"//g;
      $name = substr($s[$line], $i+7, length($s[$line]));
      $justnames{$name}++;
    }
    
    if ($i = index($s[$line], "Name_not_norm = ") != -1){
      $s[$line] =~ s/"//g;
      $name_not_norm = substr($s[$line], $i+16, length($s[$line]));
    }
    

    if ($s[$line]=~/City_check = true/) {$city{$name}++;}
    if ($s[$line]=~/Pers_check = true/) {$pers{$name}++; push (@{$per{$name}}, $name_not_norm);}
    if ($s[$line]=~/BeNom = true/) {$nom{$name_not_norm}++; $norm{$name_not_norm}=$name; push (@{$unorm{$name}},$name_not_norm);}
    if ($s[$line]=~/StrongNom = true/) {$strong{$name_not_norm}++;}
}

my $el;

my (@c, @n, %l, %t, $r, %h);
@c = keys %city;
@n = keys %nom;

foreach $el (@c){
  if ($city{$el} > $pers{$el}){
    print CITY $el;
    $l{$el}++; 
    }
}

my %ch;

foreach $el (@n){
  if (!$l{$norm{$el}} ){
    my @v = @{$unorm{$norm{$el}}};
    my $s = scalar (@v);
 #   print "$s\n";
    if (scalar (@v) == 1 || $norm{$el} eq  $el || $strong{$el} > 0) {
     print NAMES $el;
     $ch{$norm{$el}}++;
    }
    if (scalar (@v) > 1 && $norm{$el} ne $el && $strong{$el} == 0) {
      foreach $r (@v){
       $h{$r}++; 
       if ($h{$r} == 1) {
         if ($r eq $norm{$el}){
           $ch{$norm{$el}}++;
           print NAMES $r;
         } 
       }
      }
     }
     $t{$el}++;
    }
  }
my %hr;



foreach $el (keys %unorm){
  if ($ch{$el} < 1) {
    if (!$city{$el}){
      my @v = @{$unorm{$el}};
      foreach $r (@v){
        $hr{$r}++;
        if ($hr{$r} == 1) {
          print NAMES $r;
        }
      }
    }
  }
}

=head
foreach $el (keys %pers) 
{
  if (!$unorm{$el} && $pers{$el} >1 && !$city{$el}){
   for (my $i = 0, $i < scalar @{$per{$el}}, $i++){
     $p{@{$per{$el}}[$i]}++;
   }
   $persn{$el} = keys(%p);
   %p = ();

   if ($persn{$el}==1 && ($per{$el}[0]=~/goodnorm/)) { print NAMES $el;}
#   print "$el\tm---$male{$el}\tf---$female{$el}\n";}
   else {print NAMES $el;}
  }
}



