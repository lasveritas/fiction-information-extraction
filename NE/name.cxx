#encoding "utf-8"
#GRAMMAR_ROOT End 

Conj -> "и" | Comma;

//словарные имена 
FirstName -> Word<gram="sg,persn", h-reg1>;
Surname -> Word<gram="sg,famn", h-reg1>;
Patronymic -> Word<gram="sg,patrn", h-reg1>;
Nob_part -> Word <kwtype = "nob_part">;
Name -> FirstName+ | Surname | Patronymic;

As-> Word<h-reg1, gram ="nom,sg,persn,~pl,~gen,~f,m">  interp (Fact.BeNom = true;Fact.StrongNom = true);
As -> Word<h-reg1, gram ="persn", GU=&[m,f]> interp (Fact.BeNom = true;Fact.StrongNom = false); 
N -> As;

FOA -> Word<kwtype="adress", gram = "sg">; //обращение
FOA -> Word<gram = "nom, sg",kwtype="adress", GU=~[nom,acc,dat,ins,loc,gen]> interp (Fact.BeNom = true; Fact.StrongNom = true); //проверка на нач.форму, если изменяемое обращение
Anim -> Word <l-reg, gram = "sg, anim, ~persn, ~patrn,~famn ", kwtype = ~"role" >; 


SimpleAdj -> Word<gram="A"> ;
AdjCoord -> (AdvCoord) SimpleAdj<rt> (AdvCoord);
AdjCoord -> Word<gram="APRO">;
AdjCoord -> AdjCoord AdjCoord<rt>;
AdjCoord -> AdjCoord<gnc-agr[1],rt> Conj AdjCoord<gnc-agr[1]>;

AdvCoord -> Adv;
AdvCoord -> Adv Adv+;
AdvCoord -> AdvCoord Conj AdvCoord;

V -> Verb;
V -> AdvCoord Verb <rt>;

A -> FOA | Anim | Name | N |FOA N | FOA Name ;

W -> Word<~dict> | Word <gram = "S">;
GG -> W<wff=/[А-Я][А-Яа-яё\-]+/,~l-quoted, ~fw, gram="~persn, ~famn, ~patrn"> interp (Fact.Name;Fact.Name_not_norm::not_norm);
D -> GG;
//проверка на принадлежность к городам-именам
//F -> AdjPlace;
F -> Word<gram ="~INTJ,~PR">  D V <gram = "sg"> interp (Fact.Pers_check = true; Fact.BeNom = true);
F -> D V <gram = "sg", sp-agr[1]>   Word <sp-agr[1]> interp (Fact.Pers_check = true; Fact.BeNom = false );
NotPlace -> Word<kwtype=~"adjplace">;
NotSubject -> AnyWord<gram="~S"> | Word<gram="gen"> | Word<gram="acc"> | Word<gram="dat"> | Word<gram="ins"> | Word<gram="loc">;
F -> Word <gram = "nom", sp-agr[1]> NotSubject* D Verb<gram = "sg", sp-agr[1] > interp (Fact.BeNom = false);

F -> "в" interp (Fact.City_check = true) D NotPlace;
F -> A<gram="~nom"> interp (Fact.Pers_check = true ) D NotPlace;

ANomGen -> A<GU=&[nom, gen, sg, pl]>;

F -> A<gram="nom, ~gen, ~pl",GU=~[nom,acc,dat,ins,loc,gen]> interp (Fact.Pers_check = true; Fact.BeNom = true ) D NotPlace;

F -> ANomGen interp (Fact.Pers_check = true; Fact.BeNom = false ) D NotPlace;

F -> Prep A<gram="nom"> interp (Fact.Pers_check = true; Fact.BeNom = false ) D NotPlace;

F -> Word <kwtype="hyph"> D Comma interp (Fact.Pers_check = true; Fact.BeNom = true );
F -> Comma interp (Fact.Pers_check = true) D Comma;
F -> Comma interp (Fact.Pers_check = true) D EOSent;
F -> D Conj A<gram="~nom, ~geo"> interp (Fact.Pers_check = true);
F -> A<gram="~nom, ~geo"> interp (Fact.Pers_check = true) Conj D NotPlace;
F -> D Conj A<gram="nom, ~geo", GU=~["nom,acc,dat,ins,gen"]> interp (Fact.Pers_check = true; Fact.BeNom = true);
F -> A<gram="nom, ~geo", GU=~["nom,acc,dat,ins,gen"]> interp (Fact.Pers_check = true; Fact.BeNom = true) Conj D NotPlace;

F ->Prep  D "и" A<gram="nom"> interp (Fact.Pers_check = true; Fact.BeNom = false);
F ->Prep A<gram="nom"> interp (Fact.Pers_check = true; Fact.BeNom = false) "и" D NotPlace;

F -> Noun<kwtype="role"> D interp (Fact.Pers_check= false; Fact.BeNom = false) NotPlace;
F -> Nob_part D interp (Fact.Pers_check = true) NotPlace;
F -> AdjCoord<gram = "nom", GU=~[m,f]> D interp (Fact.BeNom = true; Fact.StrongNom = true) NotPlace;

Call -> "звать" | "называть";
Anaf -> "его" | "её";
F -> Call Anaf D interp (Fact.Pers_check= true) NotPlace;

PracEnd -> DD|F;

DD -> Word<h-reg1>interp (Fact.Name; Fact.Name_not_norm::not_norm; Fact.City_check=true) Word<kwset=["adjplace"]> interp (+Fact.Name; +Fact.Name_not_norm::not_norm);
DD -> Word<h-reg1, ~fw, gram = "geo"> interp (Fact.Name;Fact.Name_not_norm::not_norm;Fact.City_check = true);
DD -> Word<kwset=["adjplace"]> interp (+Fact.Name; +Fact.Name_not_norm::not_norm)  Word<h-reg1>interp (Fact.Name; Fact.Name_not_norm::not_norm; Fact.City_check=true);

End -> PracEnd;

