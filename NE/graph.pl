#!usr/bin/perl
use utf8;
use strict;
use Data::Dumper;

$Data::Dumper::Useqq = 1;{
 no warnings 'redefine';
 sub Data::Dumper::qquote {
   my $s = shift;
   return "'$s'";
 }
}

open (OUT, "../out.txt") or die "Sorry, but i can't open 'out.txt'";
open (GR, '>','graph.gv') or die "Sorry, but i can't open 'graph.gv'";
open (LIST, "wr.txt") or die "Sorry, but i can't open 'out.txt'";
open (MI_LIST, ">mi_list.txt") or die "Sorry, i can't";

binmode(OUT, ":encoding(utf-8)");
binmode(GR, ":encoding(utf-8)");
binmode(STDOUT, ":encoding(utf-8)");
binmode(STDERR, ":encoding(utf-8)");
binmode(LIST, ":encoding(utf-8)");
binmode(MI_LIST, ":encoding(utf-8)");

# word normalization
sub nrm{
        my ($b) = @_;
        $b = lc ($b);
        while ($b=~/ё/){ $b =~ s/ё/е/;}
        while ($b=~/"/){ $b =~ s/"//; }
        while ($b=~/\./){ $b =~ s/\.//;}
        while ($b=~/ /){$b =~s/ /_/;}
        while ($b=~/-/){$b =~s/-/_/;}

        return $b;
}

#named relations extraction

my @facts; #keep facts
my %f1;
my ($line, $i, $n_norm);
my %persn; #keep Rel like "norm"-> {text,text...}
my ($fir,$sec,$rel);
my %pairs;
my @text = <OUT>;
print GR "digraph G {size = 8;\n";
foreach $line (@text){
        if ($i = index($line, "FPerson") != -1)
        {
                $i = index ($line, "= ");
                $n_norm = substr ($line, $i+2,length($line)-$i-3);
                $fir = nrm($n_norm);
		chomp ($fir);
	}

        if ($i = index($line, "SPerson") != -1)
        {
                $i = index ($line, "= ");
                $n_norm = substr ($line, $i+2, length($line)-$i-3);
                $sec = nrm($n_norm);
		chomp ($sec);
        }

        if ($i = index($line, "Rel") != -1)
        {
                $i = index ($line, "= ");
                $n_norm = substr ($line, $i+2, length($line)-$i-3);
                $rel = nrm($n_norm);
		chomp($rel);
		my $para = "$fir -- $sec"; 
                if ($fir ne $sec) {
		if (($pairs{$para})&& ($rel !=~/$pairs{$para}/))
		 {$pairs{$para} = "$pairs{$para}\|$rel"}  
         	if (!$pairs{$para}){
                  $pairs{$para} = "$rel";
                }
     
	#  print GR "\"$fir\" -- \"$sec\" [label = \"$rel\",color = red]\n";}
        }
}
}

my @l= <LIST>;
my $line, my $i, my $name, my %h, my %hrev, my %qq, my %name_count, my %name_perc;
my (%f, $f, $s, $t);
for ($line = 0; $line < scalar(@l)-2 ; $line++ ){
  $f = nrm($l[$line]);
  $name_count{$f}++; 
  $s = nrm($l[$line+1]);
  $t = nrm($l[$line+2]);
  $qq{$f}++;
  my $fs = "$f -- $s";
  my $sf = "$s -- $f";
  my $ft = "$f -- $t";
  my $tf = "$t -- $f";
  chomp($f);
  chomp($s);
  chomp($t);
  
  if ((!$pairs{"$f -- $s"})&& (!$pairs{"$s -- $f"})){
          if (($f ne $s) && !($h{$sf}))  {$h{$fs}++;}
          if ($h{$sf}) {$h{$sf}++;}
  }
  if ((!$pairs{"$f -- $t"})&& (!$pairs{"$t -- $f"})){
          if (($f ne $t) && !($h{$tf})  ) {$h{$ft}++;}
          if ($h{$tf}) {$h{$tf}++;}
  }
}

my @sort = sort {$name_count{$b} <=> $name_count{$a}} keys %name_count;
#for (my $a = scalar(@sort); $a > 0; $a-- ){
#  my $n = scalar(@l)*0.05;
#  print "$n\n";
#  if ($name_count{$sort[$a]}<$n) {pop(@sort);}	
#}
#print @sort;
my $A = scalar(@sort)*0.01; print $A; 
my $B = scalar(@sort)*0.3;
for (my $el=0; $el<scalar(@sort); $el++){
  chomp($sort[$el]);
  if ($el<$A) {#print GR "\"$sort[$el]\"[height = 1, width = 3]\n ";
  $name_perc{$sort[$el]} = 1;	
  
}
  if (($el>$A)&&($el<$B)) {#print GR "\"$sort[$el]\"[height = 1, width = 2]\n "; 
  $name_perc{$sort[$el]} = 2;

}
}

my %collect;
foreach my $el (keys %pairs){
  my @e = split (" -- ", $el);
  $collect{$e[0]}++;
  $collect{$e[1]}++;
  print GR "\"$e[0]\" -> \"$e[1]\" [label = \"$pairs{$el}\", color = red]\n";
}


my (%mi,$mi);
foreach my $el (keys %h){
#  if ($h{$el}>10){
  my @s = split(" -- ", $el);
  
  if($qq{$s[0]}&& $qq{$s[1]}) {$mi = $h{$el}/scalar(keys %h)*log((($qq{$s[0]}/scalar(keys %qq))*($qq{$s[1]}/scalar(keys %qq)))/($h{$el}/scalar(keys %h)));}
 
  $mi{$el} = $mi;
#  print FIL "MI: $el -- $mi\n"; 
#  print FIL $el";";
#}

}

my @yes=sort{$mi{$b} <=> $mi{$a}}keys %mi;
for (my $i=0;$i<scalar(@yes); $i++){
   print MI_LIST "$mi{$yes[$i]}\n";


if ($mi{$yes[$i]} >= 0.1){
    my @d = split (" -- ", $yes[$i]);
    chomp ($d[0]);
    chomp  ($d[1]);
    $collect{$d[0]}++;
    $collect{$d[1]}++;
    print GR "$d[0] -> $d[1] [arrowsize = 0, penwidth = 3];\n";
}
if (($mi{$yes[$i]} > 0.05) &&($mi{$yes[$i]} < 0.1)) {
    my @d = split (" -- ", $yes[$i]);
    chomp ($d[0]);
    chomp  ($d[1]);
    $collect{$d[0]}++;
    $collect{$d[1]}++;
    print GR "$d[0] -> $d[1] [arrowsize = 0];\n";
    
  }
}

foreach my $el (keys %collect){
  
  if ($name_perc{$el} eq 1) {print GR "\"$el\" [height = 4, width = 5]\n";}
  if ($name_perc{$el} eq 2) {print GR "\"$el\" [height = 1, width = 3]\n";}

	
}

print GR "}";


