#encoding "utf-8"

WordAdjPlace -> Word<kwtype = "adjplace">;
AdjPlace -> Word<wff=/[А-Я][а-я]+(ого|ой|ая|ый|ий|ом|ое|ин|ина|ино|ее|ово|овым|овом|овой)/> WordAdjPlace;
AdjPlace -> WordAdjPlace Word<wff=/[А-Я][а-я]+(ой|ая|ый|ий|ое|ин|ина|ино|ее|ово|ого|ом|овым|овом|овой)/> ;

Place -> AdjPlace;
Place -> Word<gram = "geo">;

ROOT -> Place;
