#encoding "utf-8"
#GRAMMAR_ROOT F 

// описание человека
FIO -> AnyWord<kwtype="fio">;
FirstName -> Word<gram="sg,persn", kwtype =~"city", h-reg1>;
Surname -> Word<gram="sg,famn", kwtype =~"city", h-reg1>;
Patronymic -> Word<gram="sg,patrn", kwtype =~"city",h-reg1>;
UW -> UnknownPOS<h-reg1, ~fw, kwtype =~"city", wff=/[A-ЯЁ][a-яё]+/>; // неизвестные слова
Anim -> Word<~fw, h-reg1, gram="sg,S,anim,~patrn">; // любые одушелённые

FOA -> Word<kwtype="adress", gram = "sg">; //обращение
Anim -> Word<~fw, h-reg1, gram="S,anim,~patrn">; // любые одушелённые
Uname -> Word <kwtype ="unames">;
City -> Word <kwtype = "city">;
ShortName -> Word<wff=/[А-ЯЁ]./>;
Nob_part -> Word<kwtype = "nob_part">;

Male -> Word <kwtype = "male">;
Female -> Word <kwtype = "female">;
Dual -> Word <kwtype = "dual">;
OneWordName -> Word <kwtype = names>;

AdjCoord -> (AdvCoord) Word<gram="A",rt> (AdvCoord);
AdjCoord -> Word<gram="APRO">;
AdjCoord -> AdjCoord AdjCoord<rt>;
AdjCoord -> AdjCoord<gnc-agr[1],rt> ',' AdjCoord<gnc-agr[1]>;
AdjCoord -> AdjCoord<gnc-agr[1],rt> 'и' AdjCoord<gnc-agr[1]>;

AdvCoord -> Adv;
AdvCoord -> Adv Adv+;
AdvCoord -> AdvCoord ',' AdvCoord;
AdvCoord -> AdvCoord 'и' AdvCoord;

/*Adress -> FOA<nc-agr[1]> FirstName<nc-agr[1], rt> SpName;
Adress -> FOA<nc-agr[1]> FIO<nc-agr[1],rt >;
Adress -> FOA UW;
Adress -> FOA<nc-agr[1]> SpName<nc-agr[1],rt >;
//Adress -> Noun<gram="anim, sg", nc-agr[1]> FirstName<nc-agr[1]>;
//Adress -> Noun<gram="anim, sg", nc-agr[1]> FIO<nc-agr[1]>;
Adress -> FOA<gnc-agr[1]> Word<gnc-agr[1],h-reg1,rt>;
Adress -> FOA Anim;
Adress -> FOA Uname;
Adress -> FOA FirstName Uname;
Adress -> FOA Uname Uname;
*/
Name -> FirstName;
Name -> Surname;
Name -> Patronymic;

//Name -> FirstName Patronymic;
//A -> Word<h-reg1> Patronymic ;
//Name -> AnyWord<gram=~"persn",l-reg> Patronymic interp (Fact.Undef::not_norm);
//Name -> FIO (Name);
//Name -> Adress;
//Name -> SpName;
//Name -> Anim;
//Name -> UW;
//Name -> Word <gram ="SPRO, sg, 1p">;

A -> FOA<gram="sg">;
A -> Name<gram="sg">;
A -> Noun <gram="sg,anim">;

D -> UW interp (Fact.Undef); 
U -> UW interp (Fact.Undef; Fact.Uname::not_norm);
S -> UW interp (Fact.Uname::not_norm);

//проверка на принадлежность к городам-именам
F -> Word<gram ="~INTJ,~PR">  D Verb <gram = "3p, sg"> interp (Fact.Pers_check);
F -> "в" interp (Fact.City_check) D;
F -> A<gram="~sg,~nom"> interp (Fact.Pers_check) D;
F -> Word <kwtype="hyph"> D Comma interp (Fact.Pers_check);
F -> Comma interp (Fact.Pers_check) U Comma;
F -> Comma interp (Fact.Pers_check) U EOSent;
F -> D "и" A interp (Fact.Pers_check);
F -> A interp (Fact.Pers_check) "и" D;
F -> Word<kwtype=~"hyph"> Word <gram="geo", h-reg1> interp (Fact.City_check; Fact.Undef);
//выделение несловарных имён`:

F -> Word<gram="~PR"> A<gram = "sg, nom"> interp (Fact.Pers_check) U;
F -> Word<gram="~PR"> AdjCoord<gram="sg, nom"> S;
F -> "cказать" <gram = "3p, sg"> U;
//F -> U Name<gram="sg,nom">;
//G -> Word <kwtype =~ "hyph"> Name<gram="sg, nom,~geo",kwset =~["city"]> interp (Fact.Norm);


R -> Word<kwset=~["city", "adress"]>;
AName -> Name <kwtype =~"city">;
AName -> R <gram="sg", kwset = ["unames"]>;
AName -> Anim<kwtype=~"adress">;
F -> AName <gram="m,nom,sg"> interp (Names.Male::not_norm);
F -> AName <gram="f,nom,sg"> interp (Names.Male::not_norm);

