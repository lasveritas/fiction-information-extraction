#encoding "utf-8"
#GRAMMAR_KWSET ["places"]
#GRAMMAR_ROOT  End

NotPlace -> Word <h-reg1, GU=~["persn, famn, patrn, CONJ, PR, PART, INTJ"], kwset=~["stop","adjplace","city", "places","normnames", "adress"]>;

FirstName -> NotPlace <gram = "persn">;
FamilyName -> NotPlace <gram = "famn">;
PatrnName -> NotPlace <gram = "patrn">;
Uname -> NotPlace <kwtype = "names"> {outgram = "sg"} ;
FirstName -> Uname;
FamilyName -> Uname;
PatrnName -> Uname;
FirstName -> Norm;
FamilyName -> Norm;
PatrnName -> Norm;

FOA -> Word <kwtype = adress>;
Nob_part -> Word <kwtype = "nob_part">;
Adjec -> 'старший' | 'младший' | 'средний' | 'великий';
ShortName -> Word<wff=/[А-Я]\./>;
ShortName -> Word<wff=/(I|II|III|IV|V|VI|VII|VIII|IX|X)/>;

FamilyName ->  Nob_part NotPlace<gram="famn",rt>;
FamilyName -> Nob_part NotPlace <kwtype="names",rt>;

SimpleName -> FirstName | FamilyName | PatrnName | Uname | Norm;
Name -> FirstName<nc-agr[1],rt> FamilyName<nc-agr[1]>;
Name -> FamilyName<nc-agr[1]> FirstName<nc-agr[1],rt>; 
Name -> FirstName<c-agr[1],rt> PatrnName<c-agr[1]>;
Name -> FamilyName<nc-agr[1]> FirstName<nc-agr[1],rt> PatrnName<nc-agr[1]>;
Name -> FirstName<nc-agr[1],rt> PatrnName<nc-agr[1]> FamilyName<nc-agr[1]>;
Name -> ShortName (ShortName) FamilyName<rt>;
Name -> FirstName<rt> ShortName;
Name -> FirstName<c-agr[1],rt> FirstName<c-agr[1]>;
Norm -> Word<kwtype = "normnames">;

//Anaf -> "её" | "его" | "их" | "мой" | "моя" | "мои";
//Name -> (Anaf) Word<kwtype = "role"> Name;
//Name -> Name<sp-agr[1], rt> (Adv) Verb<sp-agr[1]>;
//Name -> Word<kwtype="role", gram ="pl"> Name "и" Name;

Name -> Word<h-reg1, gram="A", ~fw, gnc-agr[1]> Name<gnc-agr[1]>;
Name-> SimpleName;
FOAName -> FOA<c-agr[1], gram ="sg">  interp (Fact.Name) Name<c-agr[1], rt> interp (+Fact.Name);
FOAName -> FOA interp (Fact.Name) "и" FOA interp (Fact.Name) Name interp (+Fact.Name;+Fact.Name);

NameInterp -> Name<gram = "f"> interp (Fact.Name; Fact.Sex = "f");
NameInterp -> Name<gram = "m"> interp (Fact.Name;Fact.Sex = "m");
NameInterp -> Name interp (Fact.Name);

NameIn -> NameInterp;

Appos -> NameIn '('  Word interp (Fact.SameAs) ')';
Appos -> NameIn<gnc-agr[1],rt> Hyphen Word<gnc-agr[1]> interp (Fact.SameAs);
Appos -> NameIn<gnc-agr[1],rt> Comma Word<wff=/[а-я]+/, gnc-agr[1]> interp (Fact.SameAs) Comma;
Appos -> NameIn<gnc-agr[1]> Comma Word<wff=/[а-я]+/, gnc-agr[1]> interp (Fact.SameAs) '.';

Name -> SeniorJunior<gnc-agr[1]> Word<rt, gram="famn", gnc-agr[1]>;  // старший Иванов
Name -> SeniorJunior<gnc-agr[1]> Word<rt, gram="patrn", gnc-agr[1]>; // старший Петрович

Name -> SeniorJunior 'из' Word<gram="famn,pl,gen">;  // старший Иванов
Name -> SeniorJunior 'из' Word<gram="patrn,pl,gen">;
Conj -> 'и' | Comma;

//группа прилагательного

SpSimpleAdj -> SeniorJunior;
SpSimpleAdj -> DescrAdj;
SpSimpleAdj -> AnafAdj;

SeniorJunior -> 'младший' | 'старший' | 'средний'| 'первый' | 'последний';
DescrAdj -> 'великий';
AnafAdj -> 'свой' | 'его' | 'её';

A -> SimpleName<kwset =~["normnames"],GU =&[ "nom, acc, dat, gen, ins"]> interp (+Fact.Name::not_norm);
A -> SimpleName<kwset=~["normnames"], gram = "nom, acc, dat, gen, ins"> interp (+Fact.Name::not_norm);
A -> SimpleName<kwset=["normnames"], gram = "nom, acc, dat, gen, ins"> interp (+Fact.Name);
A -> SimpleName<kwset =["normnames"],GU =&[ "nom, acc, dat, gen, ins"]> interp (+Fact.Name);


//F -> SimpleName<GU =[ "nom, acc, dat, gen, ins, m"]> interp (Fact.Name::not_norm; Fact.Sex = "m");
//F -> SimpleName<GU =[ "nom, acc, dat, gen, ins, m, f"]> interp (Fact.Name::not_norm);

F -> A<c-agr[1]> A<c-agr[1]>;
F -> A;
F -> N <GU=~["nom, acc,dat,gen,ins"]>;
N -> NameIn;
N-> FOAName ;
//F -> Unchanged;

Pronoun -> Word<gram = "SPRO"> interp (Fact.Name);
Pronoun -> Word<gram="SPRO", nc-agr[1]> interp (Fact.Name)(Adj) Noun<kwtype="role", nc-agr[1]>interp (+Fact.Name);
Pronoun -> Word <gram= "APRO"> interp (Fact.Name);
Pronoun -> Word<gram="APRO", nc-agr[1]>interp (Fact.Name) (Adj) Noun<kwtype="role", nc-agr[1]>interp (+Fact.Name);

//F -> Pronoun;
End -> F;

