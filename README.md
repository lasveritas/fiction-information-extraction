**Relationship extraction from literary fiction**

The project is aimed to build a system for automatic extraction of fiction characters and relations between them. The results are shown as a graph.

**Files:**

The running script: fiction-tm/NE/do.sh

- dicts [word vocabularies]
- NE [names extraction]
- txt [collections of literary texts]
