#usr/bin/perl
use strict;
use utf8;
use Data::Dumper; 

$Data::Dumper::Useqq = 1;
{
 no warnings 'redefine';
 sub Data::Dumper::qquote
 {
   my $s = shift;
   return "'$s'";
	
 }

}

binmode(STDOUT, ":encoding(utf-8)");
binmode(STDERR, ":encoding(utf-8)");

open (FILE, "out.txt") or die "Sorry, but i can't open 'out.txt'";
binmode(FILE, ":encoding(utf-8)");
my @s=<FILE>;
my $q = 0;
open(my $f,'>','NE/graph.gv') or die "Sorry, but i can't open 'out.gv'";
binmode($f, ":encoding(utf-8)");

sub nrm{
	my ($b) = @_;
	$b = lc ($b);
	while ($b=~/ё/){ $b =~ s/ё/е/;}
        while ($b=~/"/){ $b =~ s/"//; }
        while ($b=~/\./){ $b =~ s/\.//;}
        while ($b=~/ /){$b =~s/ /_/;} 
	return $b; 
}

my @facts; #keep facts
my %f1; 
my ($line, $i, $a);
my %persn; #keep Rel like "norm"-> {text,text...}
my ($fir,$sec,$rel);
print $f "graph G {\n";
foreach $line (@s){
	if ($i = index($line, "FPerson") != -1)
	{	
		my %t = %f1;
		push @facts, \%t;
		%f1 = ();
	#	print Dumper (\@facts);
	
		$i = index ($line, "= ");
		$a = substr ($line, $i+2,length($line)-$i-3);
                $fir = nrm($a);
#	$f1{"FPerson"}= $a; 
#		push @{$persn{nrm($a)}}, nrm($a);
		
	}

	if ($i = index($line, "SPerson") != -1)
        {
                $i = index ($line, "= ");
                $a = substr ($line, $i+2, length($line)-$i-3);
                $sec = nrm($a);
 #               $f1{SPerson}= $a; 
#		push @{$persn{nrm ($a)}},nrm($a);
        }
	
	if ($i = index($line, "Rel") != -1)
        {
		$i = index ($line, "= ");
                $a = substr ($line, $i+2, length($line)-$i-3);
                $rel = nrm($a);
                 
 #               $f1{Rel}=$a;
                if ($fir ne $sec){
		print $f "\"$fir\" -- \"$sec\" [label = \"$rel\",color = red]\n";}
		             	         			
	}
   

}
#print $f "}";

