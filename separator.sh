#!/bin/sh

a=`wc -l $1`
name=$1."dir"
if [ -e "$name" ] 
 then
  echo "" 
else
 mkdir $name
fi

end=`expr index "$a" ' '`

c=`echo ${a:0:$end-1}`
b=1000;

let "part = c / b"

i=1
f=0
s=$b

while [ "$i" -le "$part" ]
do
  awk -v f=$f -v s=$s '{if (FNR>=f)print;if(FNR>=s)exit;}' $1 > $name/$1$i
  i=$(($i+1))
  f=$s
  let "s = s + $b"
done

awk -v f=$f c=$c'{if (FNR>=f)print;if(FNR>=c)exit;}' $1 > $name/$1"end"

